#!/bin/bash

# Get selected task
if [ "$#" -ne 1 ]; then
    task=""
else
    task=$1
fi

# Update system and install necessary packages
prepare_system() {
    apt-get update
    apt-get dist-upgrade -y
    apt-get install -y python3 python3-pip vim dos2unix gettext
}

# Install necessary python packages
install_python_packages() {
    pip3 install -r /vagrant/requirements.txt
}


case ${task} in
    1) prepare_system
    ;;
    2) install_python_packages
    ;;
    "") prepare_system
        install_python_packages
    ;;
esac
