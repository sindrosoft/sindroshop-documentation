Zona de administración
======================

A la zona de administración sólo pueden acceder los administradores de la tienda con su cuenta de administrador. Para
acceder a ella, hay que ir a (URL de tu tienda)/dashboard.

Al momento de acceder se presentará una pantalla de acceso que te pedirá el correo electrónico de tu cuenta de
administrador y la contraseña de dicha cuenta.

.. image:: _static/dashboard-login.png

Una vez introducidas las credenciales de acceso correctas, se mostrará la zona de administración de la tienda (a partir
de ahora el Dashboard). Aqui podrás gestionar todo lo referente a tu tienda, productos, categorías, campañas, etc.

.. image:: _static/dashboard.png

Como se puede observar, el Dashboard se compone de tres partes bien diferenciadas, las cuales son: barra superior, panel
lateral y contenido.


Barra superior
--------------

En la barra superior se mostrará tanto la localización actual, como las localizaciones relacionadas, es decir, por donde
se ha pasado para llegar hasta la localización actual. También se tiene acceso a un botón para cerrar la sesión de
administrador.

.. image:: _static/top-bar.png


Panel lateral
-------------

En la parte izquierda del Dashboard se encuentra el panel lateral. En él se puede acceder a las diferentes secciones del
propio Dashboard. Las diferentes secciones se muestran en la siguiente imagen del panel lateral desplegado completamente.

.. image:: _static/sidebar.png


Contenido
---------

Se trata de la parte central del dashboard. En el se muestra el contenido de las diferentes secciones que se vayan
seleccionando en el panel lateral. Por ejemplo, si se selecciona el apartado **Inicio**, se mostrará un resumen de la
información más relevante de la tienda en forma de gráficas, tablas y cifras.

.. image:: _static/dashboard-main.png

La mayoría de las diferentes secciones del dashboard suelen seguir la misma estructura que en la imagen anterior. Arriba
a la izquierda se muestra el nombre de la sección, a la derecha del nombre se muestran botones que realizan diferentes
acciones según la sección actual e inmediatamente debajo, se encuentran los datos de la propia sección. En ocasiones, la
información estará dividida en diferentes pestañas, si éste es el caso, las pestañas estarán justo debajo del título de
la sección y justo antes de mostrar el contenido de dicha pestaña.


# TODO: Explicar los diferentes elementos de los que etá compuesta la interfaz. Las tablas, los botones, los filtros
desplegables, etc.