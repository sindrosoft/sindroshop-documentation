.. |br| raw:: html

   <br />

Categorías
==========

Las categorías te permiten agrupar productos equivalentes de forma que fácilita a tus futuros clientes navegar por la
tienda. Esto repercute en una mayor satisfacción del cliente y una mayor predisposición a comprar. Una buena estrucutura
de categorías puede ser una de las mejores estrategias de venta.

Todos los productos visibles en la tienda deben pertenecer a una categoría y para añadir un producto, debe tener al menos
una categoría añadida a la tienda.

Si una categoría es eliminada, los productos de esa categoría pasan a una especie de categoría especial llamada
**No categoría**. Tan sólo se almacenan los productos en esa categoría para una futura referencia o para añadirlos
posteriormente a otra categoría ya que la categoría especial **No categoría** no se muestra en la tienda. Por tanto,
todos los productos que pertenezcan a ella no serán visibles en la tienda.


Listar categorías
-------------------------

Nada más entrar en el apartado **CATEGORÍAS** se mostrará una lista con todas las categorías del sistema en una interfaz
similar a la siguiente imagen.

.. image:: _static/dashboard-categories.png

En esta interfaz se pueden filtrar las categorías, ordenar la tabla según un determinado criterio o añadir y eliminar
categorías a la tienda.

Para ordenar las categorías según un criterio basta con hacer click en la cabecera de la columna por la que se quiere
ordenar. En el caso de las categorías, se pueden ordenar por la *ID* de la categoría o por el *Nombre*.


Filtrar categorías
------------------

Se pueden filtrar las categorías que se muestran según una serie de criterios. Para mostrar los filtros disponibles
basta con hacer click en el botón **Filtros** y se abrirá el siguiente desplegable.

.. image:: _static/dashboard-categories-filters.png

Las categorías se pueden filtrar por rango de *ID* y por *Nombre*. El nombre de la categoría no tiene porque ser un
valor exacto. Además, no distingue entre mayúsculas y minúsculas. Esto quiere decir que tanto :code:`jard` como
:code:`jardín` como :code:`Jardín` mostrarán la categoría *Jardín*.

.. image:: _static/dashboard-categories-filters-jardin.png

Sin embargo, el uso de las tildes si es importante. Si se busca :code:`jardin` (sin tilde en la "i"), no se mostrará
ninguna categoría.

.. image:: _static/dashboard-categories-filters-jardin-tilde.png


Añadir categorías
-----------------

Para añadir una categoría a la tienda simplemente hay que hacer click **AÑADIR CATEGORÍA**. A continuación se mostrará
la intefaz de añadir categoría. La interfaz de añadir categoría está dividida en tres pestañas. En la primera pestaña,
**INFORMACIÓN**, hay que añadir los datos básicos de una categoría:

.. image:: _static/dashboard-categories-add.png


* **Nombre:** El *nombre* de una categoría es el nombre que se mostrará tanto en la tienda como en el buscador. Por
  tanto es importante darle un nombre adecuado y significativo de lo que esa categoría contendrá.

* **Descripción:** La descripción de una categoría no se muestra en la tienda, es un dato descriptivo que sirve para
  futuras referencias. Aún así, es obligatorio establecerlo a la hora de crear una categoría.

* **Categoría padre:** Una categoría padre es una categoría que agrupa categorías, llamadas éstas subcategorías. Si
  a la hora de crear una categoría no se elige ninguna categoría padre, ella misma se convierte en una categoría
  padre y se mostrará en raiz de la tienda. Por el contrario, si se elige una categoría padre, la categoría creada
  se convierte en una subcategoría de la categoría seleccionada y se mostrará en la tienda dentro de ésta.


La pestaña a continuación, **IMÁGENES**, permite establecer las imágenes de esa categoría. Una categoría está compuesta
de dos imágenes:

.. image:: _static/dashboard-categories-add-images.png

* **Imagen de cabecera:** Es la imagen que se muestra cuando la categoría está seleccionada en la tienda.
  Normalmente se encuentra justo encima de la lista de subcategorías y de productos de la categoría.

.. image:: _static/dashboard-categories-header-sample.png

|br|

* **Imagen de miniatura:** Es la imagen que se usa cuando la categoría se muestra en una lista junto a otras

.. image:: _static/dashboard-categories-thumb-sample.png

|br|

La última pestaña, **ATRIBUTOS EXTENDIDOS** se usa para añadir atributos extendidos a la categoría.

.. image:: _static/dashboard-categories-add-attributes.png

Los atributos extendidos son propiedades que comparten todos los productos de una determinada categoría. Por ejemplo, si
se tiene la categoría *Ropa*, es normal pensar que los diferentes productos que pertenezcan a esa categoría tengan la
propiedad *Talla* o *Color*. Creando estos atributos extendidos en la categoría, al añadir más adelante un producto a la
misma, se podrá establecer el *Color* y la *Talla* de este producto.

Para añadir un **atributo extendido** a una categoría hay que hacer click en **AÑADIR NUEVO ATRIBUTO** lo que mostrará
el siguiente cuadro de diálogo.

.. image:: _static/dashboard-categories-add-attributes-dialog.png

En ese cuadro de diálogo hay que indicar el **nombre del atributo** y el **tipo de atributo**. El nombre será lo que
indique que señala ese atributo, por ejemplo *Talla* o *Color*. El tipo indica que dato va a guardar ese atributo, si
es de tipo *Texto*, es decir, una línea como mucho, o de tipo *Párrafo*, más de una línea. Más adelante, cuando se añada
un producto, se establecerá el valor de cada atributo para ese producto en concreto. Por ejemplo :code:`Color: Rojo` o
:code:`Talla: XL`.

Una vez creado el atributo extendido, simplemente pulsar en **ACEPTAR** y el atributo recién creado se mostrará en la
tabla correspondiente en la que se puede comprobar que todo ha ido correctamente, eliminar el atributo si ha habido
algún error o continuar añadiendo más atributos. Para eliminar el atributo, hay que hacer click en el ícono rojo en la
columna **ELIMINAR**.

.. image:: _static/dashboard-categories-add-attributes-table.png

Por último, para guardar una categoría solo hay que hacer click en el botón **GUARDAR** de la parte superior derecha. Si
por el contrario se quiere deshacer el trabajo, tan sólo hay que pulsar el botón **CANCELAR** para volver a la lista de
categorías sin realizar ningún cambio.


Editar categorías
-----------------

Para editar una categoría hay que hacer click en la categoría que se quiere editar en la lista de categorías.

.. image:: _static/dashboard-categories-select-edit.png

Una vez hecho esto, se presentará una interfaz igual a la de añadir categoría, pero con los datos de la categoría a
editar. A la derecha del nombre de la categoría, se indica que la categoría se está editando.

.. image:: _static/dashboard-categories-edit.png

Todo en la categoría es editable, sin embargo, si se elimina un **atributo extendido** de una categoría también se
eliminará de todos los productos asociados a esa categoría. Del mismo modo, si se añade un nuevo atributo extendido a la
categoría, los productos asociados a esa categóría tendrán ese atributo pero con su valor en blanco.

Una vez realizados los cambios necesarios a los campos que correspondan y hay que pulsar en **GUARDAR** en la parte
superior derecha para guardar los cambios. Si por el contrario se quiere volver atrás sin realizar ningún cambio hay que
pulsar el botón **CANCELAR** en la parte superior derecha para regresar a la lista de categorías.


Eliminar categoría
------------------

La eliminación de una categoría es un proceso irreversible, por tanto se recomienda hacerlo con mucho cuidado. Para
eliminar categorías hay que seleccionar en la lista de categorías las categorías que se desean eliminar marcando el
*checkbox* de la primera columna. Posteriormente, haciendo click en **ELIMINAR CATEGORÍA** se eliminaran las categorías
seleccionadas.

.. image:: _static/dashboard-categories-delete.png

Hay que recordar que si una categoría es eliminada, los productos de esa categoría pasan a una especie de categoría
especial llamada **No categoría** y por tanto, no se muestran en la tienda. Estos productos pueden posteriormente
añadirse a otra categoría o eliminarse.


Errores
-------

Durante la creación o edición de una campaña pueden ocurrir errores. Cuando esto suceda, normalmente al intentar guardar
cambios, nos saldrá un mensaje de alerta que nos indicará que se han producido errores. Además, los campos que hayan
tenido errores apareceran resaltados en rojo y con un mensaje justo debajo que indicará el motivo del error.

.. image:: _static/dashboard-categories-errors.png

En la imagen se puede ver que hay un error tanto en el nombre de la categoría como en la descripción. El mensaje de
error indica que ambos campos son obligatorios. Por tanto, para corregir estos errores hay que poner un nombre y una
descripción a la categoría.

Para corregir los errores normalmente basta con poner el valor correcto en el campo con el error y volver a intentar
guardar los cambios.