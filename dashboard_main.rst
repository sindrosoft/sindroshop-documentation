Inicio
======

Esta sección se presenta cada vez que se accede al Dashboard y en ella se puede ver toda la información relevante de la
tienda de forma gráficos, tablas y cifras.

.. image:: _static/dashboard-main.png

En la zona de botones, en la esquina superior derecha, hay un botón que permite seleccionar el rango de fechas que se
usará para seleccionar los datos a mostrar. Justo debajo del título de la sección, en este caso **Inicio**, se puede
ver el rango de fechas seleccionado actualmente.

.. image:: _static/dashboard-main-select-date.png