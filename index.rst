¡Bienvenido a la documentación de Sindroshop!
=============================================

Sindroshop es un sistema eCommerce de código abierto escrito en Python y desarrollado por Sindrosoft. Usa una base de
datos PostgreSQL. Esta guía ha sido escrita tanto para ayudarte a descargar e instalar Sindroshop como para ayudarte a
configurar tu tienda una vez instalada.

La documentación de Sindroshop está dividida en dos secciones: una para los usuarios y otra para los desarrolladores.


Soporte
=======

Por favor, usa el sistema de `seguimiento de incidencias`_ en Bitbucket para reportar fallos o problemas. Antes de abrir una nueva
incidencia, asegúrate de que no existe ya una previamente tratando el mismo tema.

.. _seguimiento de incidencias: https://bitbucket.org/sindrosoft/sindroshop-documentation/issues


Características
===============

* asdfad
* asdfasdfad
* asdfadf


Documentación usuarios
======================
.. toctree::
    :maxdepth: 2

    user_intro
    dashboard
    dashboard_main
    dashboard_categories
    dashboard_items
    dashboard_campaigns
    dashboard_customers
    dashboard_sales
    dashboard_management


Documentación desarrolladores
=============================
.. toctree::
    :maxdepth: 2